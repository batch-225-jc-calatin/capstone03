import './App.css';
import {useState, useEffect} from 'react'
import {UserProvider} from './UserContext'
import AppNavbar from './components/AppNavbar'
import ArchievingFalse from './components/ArchievingFalse'
import ArchievingTrue from './components/ArchievingTrue'
import UpdateProduct from './components/UpdateProduct'
import AdminDashBoard from './components/AdminDashBoard'
import Home from './pages/Home'
import Courses from './pages/Courses'
import DashBoard from './pages/DashBoard'
import CourseView from './components/CourseView'
import Register from './pages/Register'
import AddProduct from './pages/AddProduct'
import Login from './pages/Login'
import Logout from './pages/Logout'
import ErrorPage from './pages/ErrorPage'
import { Container } from 'react-bootstrap'
import Carousel from './components/Highlights'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }
      useEffect(() => {
      fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
        headers: {
          Authorization: `Bearer ${ localStorage.getItem('token') }`
        }
      })
      .then(res => res.json())
      .then(data => {
         if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });

        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        }
      })

      }, []);

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/product/Active" element={<Courses/>}/>
              <Route path="/product/:productId" element={<CourseView/>}/>
              <Route path="/product/archieve/:productId" element={<ArchievingFalse/>}/>
              <Route path="/product/unarchieve/:productId" element={<ArchievingTrue/>}/>
              <Route path="/product/updating/:productId" element={<UpdateProduct/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/addProduct" element={<AddProduct/>}/>
              <Route path="/product/" element={<DashBoard/>}/>
              <Route path="/logout" element={<Logout/>} />
              {/*<Route path="/*"  element={<ErrorPage/>}/>*/}
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>


  );
   <Carousel />
}


export default App;

