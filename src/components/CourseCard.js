import {Card, CardImg} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import Container from 'react-bootstrap/Container';
import CardGroup from 'react-bootstrap/CardGroup';
import UserContext from '../UserContext'
import {useContext} from 'react'
export default function CourseCard(product){

  // Destructuring the props
  const {name, description, price,  _id, isActive} = product.productProp



  return(
    <>
    
    
    <Container fluid >
    
      <Card  class ="courseCards"  >
      <Card.Body>
        <Card.Header>{name}</Card.Header>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PHP {price}</Card.Text>
        

         
        
        <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
      </Card.Body>
       </Card>
       
      </Container>
   
    </>
  )
}

