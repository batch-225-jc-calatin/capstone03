	import Navbar from 'react-bootstrap/Navbar'
	import Nav from 'react-bootstrap/Nav'
	import { Link, NavLink } from 'react-router-dom'
	import {useContext} from 'react'
	import UserContext from '../UserContext'
	import Container from 'react-bootstrap/Container';
	import Stack from 'react-bootstrap/Stack';
	import CourseCard from '../components/CourseCard'
	import AdminDashBoard from '../components/AdminDashBoard'
	
	import '../App.css';
	export default function AppNavbar(){

		const {user} = useContext(UserContext)

		return(
			
			<Navbar class= "navbarBox" bg="dark" expand="lg" variant="dark" pb="3">
			<Container fluid>
			<Stack direction="horizontal" gap={5}>
			<Navbar.Brand as={Link} to="/">Just Click</Navbar.Brand>
			 <div className="vr" />
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="ml-auto">


						<Nav.Link class="home" as={NavLink} to="/">Home</Nav.Link>
						<Nav.Link as={NavLink} to="/product/Active">Courses</Nav.Link>
						

						

						{(user.id  ) ?
						<>
							
							{(user.isAdmin  && 
								<>
                             <Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
							<Nav.Link as={NavLink} to="/product/">AdminDashBoard</Nav.Link>
							
							</>
                        )}
     						
							<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
						</>

						:
						<>

							
						<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>


					
						
						</>}

					

 						
						
					</Nav>
				</Navbar.Collapse>
				</Stack>
				</Container>
			</Navbar>
			
		)

	}

