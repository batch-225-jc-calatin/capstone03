import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardImg } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Archieve(product) {

	// connected to :userid
	const {productId} = useParams()
	const {user} = useContext(UserContext)
	const navigate = useNavigate()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(0);
	const [image, setImage] = useState("");

	const unarchieve = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/product/unarchieve/${productId}`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Product is Now Available!"
				})

				navigate('/product')
			} else {
				console.log(result)

				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	}

	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(response => response.json())
		.then(result => {
			console.log(result.name)
			setName(result.name)
		
		})
	},[productId])

	return(
	<>
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 4, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							

							{	
									<Button variant="primary" onClick={() => unarchieve(productId)}>Available</Button>
								
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	</>
	)
}




