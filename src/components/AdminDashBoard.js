
import {Card, CardImg} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import Container from 'react-bootstrap/Container';
import CardGroup from 'react-bootstrap/CardGroup';
import UserContext from '../UserContext'
import {useContext} from 'react'
import Table from 'react-bootstrap/Table';
import CourseCard from '../components/CourseCard'

export default function AdminDashBoard(product){

  
 const {name, description, price,  _id, isActive} = product.productProp



  return (
    
          
             
    <Table striped bordered hover variant="dark" responsive>
      {/*<thead>
        <tr>
          <th>Product Id</th>
          <th>Product Name</th>
          <th>Description</th>
          <th>Price</th>
         
        </tr>
      </thead>*/}
     

      
      <tbody>
        <tr>
          <td>{_id}</td>
          <td>{name}</td>
          <td>{description}</td>
          <td>{price}</td>
          <td>{isActive}</td>
          { (isActive) ?
            <td>Available</td>
            :
            <td>UnAvailable</td>
  
         }
          <td></td>
          <td></td>
          
          <td><Link className="btn btn-success" to={`/product/unarchieve/${_id}`}>Available</Link></td>
          <td><Link className="btn btn-dark" to={`/product/archieve/${_id}`}>UnAvailable</Link></td>
          <td><Link className="btn btn-danger" to={`/product/updating/${_id}`}>Update</Link></td>
        </tr>
        
      </tbody>
    </Table>

  

 
  );




}

