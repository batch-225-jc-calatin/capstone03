
import { Button, Row, Col } from 'react-bootstrap';

export default function Banner() {
			    return (
	                <Row>
	                	<Col>
		                    <h1>Just Click "Photography Online BootCamp"</h1>
		                    <h4>"It's not enough to just own a camera. Everyone owns a camera. To be a photographer, you must understand, appreciate, and harness the power you hold!".</h4>
		                    
		                </Col>
	                </Row>
			    )
}