import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardImg, Form } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Archieve(product) {

	// connected to :userid
	const {productId} = useParams()
	const {user} = useContext(UserContext)
	const navigate = useNavigate()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState(0);
	const [image, setImage] = useState("");

	const unarchieve = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/product/updating/${productId}`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
			})
			
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Product is Now Updated!"
				})

				navigate('/product')
			} else {
				console.log(result)

				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	}

	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(response => response.json())
		.then(result => {
			


					setName(result.name)
					setDescription(result.description)
					setPrice(result.price)
		
		})
	},[productId])

	return(
	<>
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 4, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Form.Group controlId="name">
				        		<Form.Label>Product Name</Form.Label>
					        	<Form.Control 
					            type="text" 
					            placeholder="Enter Product Name"
					            value={name} 
					            onChange={event => setName(event.target.value)}
					            required/>
			    			</Form.Group>

							<Form.Group controlId="description">
				        		<Form.Label>Product Description</Form.Label>
					        	<Form.Control 
					            type="text" 
					            placeholder="Enter Product Description"
					            value={description} 
					            onChange={event => setDescription(event.target.value)}
					            required/>
			    			</Form.Group>

							<Form.Group controlId="price">
				        		<Form.Label>Product Price</Form.Label>
					        	<Form.Control 
					            type="text" 
					            placeholder="Enter Product Price"
					            value={price} 
					            onChange={event => setPrice(event.target.value)}
					            required/>
			    			</Form.Group>

							{	
									<Button variant="primary" onClick={() => unarchieve(productId)}>Update</Button>
								
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	</>
	)
}




