import CourseCard from '../components/CourseCard'
import AdminDashBoard from '../components/AdminDashBoard'

import Loading from '../components/Loading'
import {useEffect, useState} from 'react'
// gagawa ng sarile para sa course card para sa get all active tapos ito para sa admindashboard
export default function Product(){
	
const [product, setProduct] = useState([])
const [isLoading, setIsLoading] = useState(true)
		useEffect((isLoading) => {

	fetch(`${process.env.REACT_APP_API_URL}/product/getActive`)
	.then(response => response.json())
	.then(result => {
	console.log(result)
	setProduct(
		result.map(product => {
				return ( 
					
					
					<CourseCard key={product._id} productProp ={product}/>

					

					 
					)
				})
			)
			setIsLoading(false)
		})
	}, [])

	return(
			
				(isLoading) ?
					<Loading/>
			:
				<>
					{product}
				</>
	)
}
