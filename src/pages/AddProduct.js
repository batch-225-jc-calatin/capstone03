import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function AddProduct() {

	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [isActive, setIsActive] = useState(false)

	
	
    	function addProduct(event){
			event.preventDefault()

			// const retrieveUser = (token) => {
   //      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
   //         	 	headers: {
   //              Authorization: `Bearer ${token}`
   //          }
   //      })

   //      .then(response => response.json())
   //      .then(result => {
   //          	setUser({
   //              id: result._id,
   //              isAdmin: result.isAdmin
   //          	})
   //      	})
   //  	}

		fetch(`${process.env.REACT_APP_API_URL}/product/checkProduct`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true) {
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Product already exist!'
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/product/addProduct`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json',Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price,
					})
				})
				.then(response => response.json())
				.then(result => {
					console.log(result);
					
					setName('');
					setDescription('');
					setPrice('');
					

					if(result){
						Swal.fire({
							title: 'Product Added Successful!',
							icon: 'success',
							text: 'Salamat sa pag papayaman!'
							})
						
						// navigate('/addProduct')

					} else {
						Swal.fire({	
							title: 'Product Added Failed',
							icon: 'error',
							text: "Tropa, mukang mali ang iyong inilagay! :("
						})
					}		
				})
			}
		})

	}

	useEffect(() => {
		if((name !== '' && description !== '' && price.length !== 1 )){
			
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price])

	return (
		(user.id ===null) ?
			<Navigate to="/"/>
		:
			<Form onSubmit={event => addProduct(event)}>

				<Form.Group controlId="name">
			        <Form.Label>Product Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Product Name"
				            value={name} 
				            onChange={event => setName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="description">
			        <Form.Label>Product Description</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Product Description"
				            value={description} 
				            onChange={event => setDescription(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="price">
			        <Form.Label>Product Price</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Product Price"
				            value={price} 
				            onChange={event => setPrice(event.target.value)}
				            required
				        />
			    </Form.Group>

	            {	isActive ? 
	            		<Button variant="primary" type="submit" id="submitBtn">
			            	Submit
			            </Button>
			            :
			            <Button variant="primary" type="submit" id="submitBtn" disabled>
			            	Submit
			            </Button>
	            }
	            
			</Form>
	)

	}