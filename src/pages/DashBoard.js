
import AdminDashBoard from '../components/AdminDashBoard'
import CourseCard from '../components/CourseCard'
import Loading from '../components/Loading'
import {useEffect, useState} from 'react'

export default function Dashboard(){
	
const [product, setProduct] = useState([])
const [isLoading, setIsLoading] = useState(true)
		useEffect((isLoading) => {

	fetch(`${process.env.REACT_APP_API_URL}/product/`)
	.then(response => response.json())
	.then(result => {
		  console.log(result)
	setProduct(
		result.map(product => {
				return (
					(AdminDashBoard)?
					
					<AdminDashBoard key={product._id} productProp ={product}/>

					:
					<CourseCard key={product._id} productProp ={product}/>

					
					)
				})
			)
			setIsLoading(false)
		})
	}, [])

	return(
			
				(isLoading) ?
					<Loading/>
			:
				<>
					{product}
				</>
	)
}
